'use client';
import React, { useEffect } from "react";
import { useQuery } from "@tanstack/react-query"
import { getCharactersData } from "@/services";
import Error from "@/components/error/error";
import Loading from "@/components/loading/loading";
import CharactersList from "@/components/characters-list/characters-list";
import { ButtonLink } from "@/components/ui/button-link";
import { ChevronLeft, ChevronRight } from "@/components/icons";


export default function Characters({searchParams}:{searchParams:{page:number}}) {
    // Початковий номер сторінки.
    useEffect(() => {
        searchParams.page=1
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    // Отримання данних 
    const{data:characters, isLoading, isError}= useQuery({
        queryKey:['characters','info', searchParams.page],
        queryFn: ()=>getCharactersData(searchParams.page),
    })
    // Функція клік на попередню сторінку.
    const prevPage = () => {
        if (searchParams.page > 1) {
            return +searchParams.page -1
        }else return 1
    }
    // Функція клік на наступну сторінку.
    const nextPage = () => {
        if (characters?.info.next) {
            return +searchParams.page + 1
        }else return searchParams.page
    }  
    // Індікатор завантаження.
    if(isLoading){
        return <Loading/>
    }
    // Помилка.
    if(isError){
        return <Error/>
    }
    // Вивід данних.
    return (
        <main className='p-6'>
            <h1 className='font-bold text-2xl m-3 text-green-500'>Characters</h1>
           {characters && <CharactersList characters={characters?.results}></CharactersList>}
            <ul className='flex w-auto justify-between my-4'>
                 <li>
                    <ButtonLink className='text-green-500 disabled:opacity-40 hover:-translate-x-1 bg-inherit'  disabled={+searchParams.page===1} href={`/characters/?page=${prevPage()}`} >
                    <ChevronLeft></ChevronLeft>
                        Prev</ButtonLink>
                </li>
                <li>
                    <ButtonLink className='bg-inherit text-green-500 disabled:opacity-40 hover:translate-x-1' disabled={+searchParams.page === characters?.info.pages} href={`/characters/?page=${nextPage()}`} >Next
                    <ChevronRight></ChevronRight> 
                    </ButtonLink>
                </li>
            </ul>
        </main>    
    )
}