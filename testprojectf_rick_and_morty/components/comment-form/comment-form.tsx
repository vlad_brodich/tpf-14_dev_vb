import { Button } from '@/components/ui/button'
import { 
    Dialog, 
    DialogBody, 
    DialogContent, 
    DialogFooter, 
    DialogHeader, 
    DialogTrigger 
    } from '@/components/ui/dialog'
import { Input } from '../ui/input'
import { SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import useFavoritesStore from '@/store';


const schema = z.object({
	username:z.string().min(2, { message: 'Username is too short' }).max(20, 'Username is too long').transform((v) => v.toLowerCase().replace(/\s+/g, '_')),
	email: z.string().min(2).email('Incorrect email'),
	text: z.string().min(2,{message: 'write a comment'}).max(300),

});

type FormSchema = z.infer<typeof schema>

type useId ={
	useId:number
}

export default function СommentForm({useId}:useId):JSX.Element {
 const {
    register,
    handleSubmit,
    reset,
	formState: { isDirty, isSubmitting, errors }
  } = useForm<FormSchema>({
    resolver: zodResolver(schema)
  });

	const characters = useFavoritesStore((state) => state.characters);
	const addComment = useFavoritesStore((state) => state.addComment);

	const onSubmit: SubmitHandler<FormSchema> = (data) => {
		addComment(useId,{...data,date:new Date().toDateString()})
		console.log(data)
		reset()
	  }

    return (
        <div>
            <Dialog>
	<DialogTrigger className='text-white text-xl hover:text-blue-500'>Add Сomment</DialogTrigger>
	<DialogContent>
		<DialogHeader title="Add Сomment"/>
			<DialogBody>
				<form onSubmit={handleSubmit(onSubmit)} >	
					<Input variant={errors.username? 'error':isDirty?'success':'primary'} {...register("username", { required: true })}
					name='username'	className='block m-2' placeholder='username' type='text'></Input>

					{errors.username && (
					<span role='alert' >
						{errors.username?.message}
					</span>
					)}	
							
					<Input variant={errors.email?'error':isDirty?'success':'primary'}  
					{...register("email", { required: true })}
        			name="email"
        			type="email" className='block m-2' placeholder='email'></Input>

					{errors.email && (
					<span role='alert'>
						{errors.email?.message}
					</span>
					)}

					<textarea style={{resize:'none'}} {...register("text", { required: true })}
						name='text' className='block my-3 bg-input-primary p-3 rounded-lg w-full' placeholder='Comment text'></textarea>

				<Button disabled={!isDirty || isSubmitting}>Confirm</Button>			
			</form>
		</DialogBody>
	</DialogContent>
</Dialog>

        </div>
    )
}