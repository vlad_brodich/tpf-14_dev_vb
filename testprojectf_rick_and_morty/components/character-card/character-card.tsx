import Image from "next/image";
import { characterProps } from "./character.props";
import useFavoritesStore from "@/store";
import { usePathname } from "next/navigation";
import { elIsCharacters } from "@/function";
import star from '../../app/image/star.svg'

export default function CharacterCard({id, name, image,status,species}:characterProps):JSX.Element{
    const pathname = usePathname()
    const charactersFavorirt = useFavoritesStore((state)=>state.characters)

    return(
        <>
        {image && <div className="relative h-[180px] w-full">
                <Image src={image} alt={name} fill sizes="300px" className="object-cover " priority={true} />
            {elIsCharacters(charactersFavorirt, id) ?
                    <div className='border border-green-200 absolute -top-6  -right-6 w-10 rounded-full p-1 bg-gray-500'>
                        <Image width={40} src={star} alt="star"></Image>
                    </div>:null}   
        </div>}
            <div className="flex flex-col">
                <h2 className='text-xl text-center text-blue-600 font-bold'>{name}</h2>
                <p className='font-light'>Status: <span className='font-medium'>{status}</span></p>
                <p className='font-light'>Species: <span className='font-medium'>{species}</span></p>
            </div>
        </>
    )
}